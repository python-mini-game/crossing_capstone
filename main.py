import time
from turtle import Screen
from player import Player
from car_manager import CarManager
from scoreboard import Scoreboard

screen = Screen()
screen.setup(width=600, height=600)
screen.tracer(0)
screen.listen()

player = Player()
screen.onkey(player.move_forward, "w")

car = CarManager()

scoreboard = Scoreboard()

game_is_on = True
while game_is_on:
    time.sleep(car.move_speed)
    screen.update()
    
    for cars in car.all_cars:
        if cars.distance(player) < 20:
            scoreboard.game_over()
            game_is_on=False
    if player.ycor() > 290 :
        player.goto(0, -280)
        scoreboard.player_score()
        car.move_speed *= 0.9
        
            
        
    
    car.great_car()
    car.move()
    
    
screen.exitonclick()
